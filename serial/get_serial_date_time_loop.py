# ttyAMCO.py
# nohup python ttyAMCO-file-loop.py &
# ps aux|grep python
# kill <the pid>
import sys,os,serial
from datetime import datetime
import time

def getTimeString():
	now = datetime.now() # current date and time
	timeString = now.strftime("%Y%m%d-%H%M%S")
	#print("date and time:",timeString)
	return timeString[2:len(timeString)]

	
def main():
	# create directory and cd into that directory
	nowstr = getTimeString()
	print(nowstr)
	serial_port = '/dev/ttyACM0';
	baud_rate = 9600; #In arduino, Serial.begin(baud_rate)
	ser = serial.Serial(serial_port, baud_rate)
	t_end = time.time() + 15 
	while time.time() < t_end:
		line = ser.readline();
		#line = line.decode("utf-8") #ser.readline returns a binary, convert to string
		#print(line,end="")
		linestr = str(line)
		#print(linestr)
		lineclean = linestr.replace("b","")
		lineclean = lineclean.replace("\'","")
		lineclean = lineclean.replace("\\r","")
		lineclean = lineclean.replace("\\n","")
		#print(lineclean)
		#sys.stdout.write(linestr)
		lineInt = int(lineclean)
		#print("lineInt ",lineInt)
		if (lineInt < 154):
			#print("cm<154")
			#cmd0 = "raspistill -o now.png"
			nowstr = getTimeString()
			print("cm<154 "+nowstr)
			#time.sleep(1)
			#lineInt = 100000

		
if __name__ == '__main__':
	main()

#include <iostream>
#include <string>
using namespace std;

int main()
{
	string colors[5] = {"FDCBA8","FF0000", "00FF00", "0000FF", "FFFF00"};
	string hex_str= "";
	string red, green , blue;
	int r =0,g = 0,b = 0;
	int n;

	for (n= 0; n  <5;n++){
		hex_str = colors[n];
		cout<<"hex_str: "<<hex_str<<" "<<n<<endl;
		red = hex_str.substr(0,2);
		green = hex_str.substr(2,2);
		blue = hex_str.substr(4,2);
		cout<<red<<" "<<green<<" "<<blue<<endl;
		r = stoi(red, 0, 16);
		g = stoi(green, 0, 16);
		b = stoi(blue, 0, 16);
		cout<<"rgb:  "<<r<<" "<<g<<" "<<b<<"\n\n";

		
	}

	return 0;
}

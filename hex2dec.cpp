#include <stdlib.h>
#include <iostream>

int main()
{
   char const* str = "#FF9922";
   char red[5] = {0};
   char green[5] = {0};
   char blue[5] = {0};

   red[0] = green[0] = blue[0] = '0';
   red[1] = green[1] = blue[1] = 'X';

   red[2] = str[1];
   red[3] = str[2];

   green[2] = str[3];
   green[3] = str[4];

   blue[2] = str[5];
   blue[3] = str[6];

   int r = strtol(red, NULL, 16);
   int g = strtol(green, NULL, 16);
   int b = strtol(blue, NULL, 16);

   std::cout << "Red: " << r << ", Green: " << g << ", Blue: " << b << std::endl;
}

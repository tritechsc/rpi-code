#include <iostream>
#include <string>
using namespace std;

int main()
{
	//
	string hex_str= "";
	string red ="", green="" , blue="";
	int r =0,g = 0,b = 0;


	hex_str = "FDCBA9";
	cout<<"hex_str: "<<hex_str<<endl;
	red = hex_str.substr(0,2);
	green = hex_str.substr(2,2);
	blue = hex_str.substr(4,2);
	cout<<red<<" "<<green<<" "<<blue<<endl;
	r = stoi(red, 0, 16);
	g = stoi(green,0, 16);
	b = stoi(blue, 0, 16);

	cout<<"red:  "<<r<<endl;
	cout<<"green:"<<g<<endl;
	cout<<"blue: "<<b<<endl;

	return 0;
}

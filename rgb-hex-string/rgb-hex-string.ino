//This works
//c++ headers

int redPin = 2;
int greenPin = 3;
int bluePin = 4;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

String colors[5] = {"FDCBA9","FF0000", "00FF00", "0000FF", "FFFF00"};
String hexcolor = "";
String hex_str= "";
  String red, green , blue;
  int r =0,g = 0,b = 0;
  int n =0;
  int count = 0;
/////////////////////////
void loop() {
  if (count == 0){
    for (n = 0; n < 5;n++){
    hexcolor = colors[n];
     setColor(hexcolor); 
     delay(1000);
    }
    count = 1;
  }
} //end loop


int redValue; int greenValue; int blueValue;
char hexValue[2];

void setColor(String rgb) {
  red = rgb.substring(0,2);
  green = rgb.substring(2,4);
  blue = rgb.substring(4,6);
   Serial.print(red+" "+green+" "+blue);
   Serial.print(" * * * * \n\n");
   //red
   hexValue[0] = red[0]; hexValue[1] = red[1];
   redValue = (int)hexValue[0] +(int)hexValue[1]; 
   //green
   hexValue[0] = green[0]; hexValue[1] = green[1];
   greenValue = (int)hexValue[0] +(int)hexValue[1]; 
   //blue
   hexValue[0] = blue[0]; hexValue[1] = blue[1];
   blueValue = (int)hexValue[0] +(int)hexValue[1]; 
   
   Serial.println(redValue);
  Serial.println(greenValue); 
  Serial.println(blueValue); 
  
   
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
